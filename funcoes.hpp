#ifndef FUNCAO_HEADER
#define FUNCAO_HEADER

#include <iostream>
#include <math.h>
#include <string>
#include <vector>


/*
 * As linhas abaixo servem para ajustar alguns comandos especificos do sistema
 * operacional, dessa forma o codigo pode ser portado entre Linux e Windows com
 * mais facilidade.
 */
#if defined(_WIN32)
#define limparTela system("cls")
#define esperar system("pause")
#elif defined(__linux__)
#define limparTela system("clear")
#define esperar system("read null")
#endif


using namespace std;

typedef struct  {
    int conta;
    string nome;
    double saldo;
    int senha;
} Cliente ;

//vector <Cliente> clientes_;

Cliente cadastrarConta();
void acessarConta(vector <Cliente> clientes_);

void sacar(Cliente cliente, double valor);

void depositar(Cliente cliente, double valor);

void transferir(Cliente origem, Cliente destino, double valor);

void verExtrato(Cliente cliente);
void saldoTotal(Cliente saldo  ); 
char opcaoMenu();

// BancoDados acessarConta(bancoDados Usuario); // OK

void menuCliente(Cliente cliente);
//void menuInicial(vector <Cliente> clientes_);

#endif

/* struct BancoDados {
    static const int MAXIMO = 50;
    int quantidadeCadastrada = 0;
    Cliente clientes[MAXIMO];

    void adicionar(BancoDados banco, Cliente cliente) {}
    void listar() {}
}; */